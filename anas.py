import spacer as s
dict1 = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4,
         'F': 5, 'G': 6, 'H': 7, 'I': 8, 'J': 9,
         'K': 10, 'L': 11, 'M': 12, 'N': 13, 'O': 14,
         'P': 15, 'Q': 16, 'R': 17, 'S': 18, 'T': 19,
         'U': 20, 'V': 21, 'W': 22, 'X': 23, 'Y': 24, 'Z': 25}

d1 = {}

alp = []

for i in dict1:
    d1[i] = dict1[i] + 1
    alp += i

dict2 = {0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E',
         5: 'F', 6: 'G', 7: 'H', 8: 'I', 9: 'J',
         10: 'K', 11: 'L', 12: 'M', 13: 'N', 14: 'O',
         15: 'P', 16: 'Q', 17: 'R', 18: 'S', 19: 'T',
         20: 'U', 21: 'V', 22: 'W', 23: 'X', 24: 'Y', 25: 'Z'}
d2 = {}

for i in dict2:
    d2[i + 1] = dict2[i]

def mod(a, B):
    return a%B

dict1, dict2 = d1, d2

def encrypt(msg, key = 7):
    msg, loc = s.spacer(msg)
    msg = msg.upper()
    st = ''
    for i in msg:
        temp =dict1[i] + mod(dict1[i], key)
        y = temp%26
        if y == 0:
            st += 'A'
        else:
            st += dict2[y] 
    return s.despacer(st , loc)

def decrypt(msg, key= 7):
    msg, loc = s.spacer(msg)
    msg = msg.upper()
    out = ''
    for i in msg:
        for j in alp:
            if encrypt(j, key) == i:
                out += j
            else:
                continue
    return s.despacer(out, loc)

def encrypt_file(file_in, file_out, key):
	f = open(file_out, 'a')
	with open(file_in, 'r') as filetoberead:
		j = filetoberead.readlines()
		for i in j:
			t = encrypt(i.rstrip(), key)
			f.write(t)
			f.write('\n')
	f.close()

def decrypt_file(file_in, file_out, key):
	f = open(file_out, 'a')
	with open(file_in, 'r') as filetoberead:
		j = filetoberead.readlines()
		for i in j:
			t = decrypt(i.rstrip(), key)
			f.write(t)
			f.write('\n')
	f.close()
