# Implementation of Affine Cipher in Python 
import spacer
# Extended Euclidean Algorithm for finding modular inverse 
# eg: modinv(7, 26) = 15 
def egcd(a, b): 
	x,y, u,v = 0,1, 1,0
	while a != 0: 
		q, r = b//a, b%a 
		m, n = x-u*q, y-v*q 
		b,a, x,y, u,v = a,r, u,v, m,n 
	gcd = b 
	return gcd, x, y 

def modinv(a, m): 
	gcd, x, y = egcd(a, m) 
	if gcd != 1: 
		return None # modular inverse does not exist 
	else: 
		return x % m 

# affine cipher encrytion function 
# returns the cipher text 
def encrypt(text, key): 
	''' 
	C = (a*P + b) % 26 
	'''
	text, loc = spacer.spacer(text)
	
	val = [ chr((( key[0]*(ord(t) - ord('A')) + key[1] ) % 26) 
				+ ord('A')) for t in text.upper().replace(' ','') ]
	valeria = spacer.despacer(val, loc)
	
	return ''.join(valeria) 


# affine cipher decryption function 
# returns original text 
def decrypt(cipher, key): 
	''' 
	P = (a^-1 * (C - b)) % 26 
	'''
	text, loc = spacer.spacer(cipher)
	val = [ chr((( modinv(key[0], 26)*(ord(c) - ord('A') - key[1])) 
					% 26) + ord('A')) for c in text.upper() ]
	valeria = spacer.despacer(val, loc)
	return ''.join(valeria) 

def encrypt_file(file_in, file_out, key):
	f = open(file_out, 'a')
	with open(file_in, 'r') as filetoberead:
		j = filetoberead.readlines()
		for i in j:
			t = encrypt(i.rstrip(), key)
			f.write(t)
			f.write('\n')
	f.close()

def decrypt_file(file_in, file_out, key):
	f = open(file_out, 'a')
	with open(file_in, 'r') as filetoberead:
		j = filetoberead.readlines()
		for i in j:
			t = decrypt(i.rstrip(), key)
			f.write(t)
			f.write('\n')
	f.close()
